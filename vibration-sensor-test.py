#
# http://www.d3noob.org/2023/04/sensing-vibration-with-raspberry-pi-pico.html
# 
# working
#
# needs 3.3v power ground and GPIO pin
# 
from machine import ADC, Pin
import time

# Sensor reading connection 
adc = ADC(Pin(28))
VIBRATION_THRESHOLD = 600
STOP = False

try:
    print("Tap the glass")
    
    while not STOP:
        vibration = adc.read_u16()
        
        if(vibration > VIBRATION_THRESHOLD):
            print(f"vibration!! ={vibration}")
            
        time.sleep(0.05)

except KeyboardInterrupt:
    print('STOP!! Got ctrl-c')
    STOP = True
