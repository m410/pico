# https://github.com/geeekpi/upsplus
#
# NOT working
#
import machine
import time

rxPin = machine.Pin(1)
txPin = machine.Pin(0)
upsUart = machine.UART(0, baudrate=9600, rx=rxPin, tx=txPin)
STOP = False

try:
    upsUart.init()
    print('uart init')
    
    while not STOP:
        data = upsUart.readline()
        print('data: {}'.format(data))
        
        if data != None:
            decode = data.decode('utf-8')
            normal = decode.rstrip('\n')
            
            if '|' in normal:        
                print("Battery Voltage: {}mV".format(normal.split('|')[0]))
                print("Current Charing in: {}mA".format(normal.split('|')[1]))
                battery_current = float(normal.split('|')[2] / 10.0 )
                print("Consuming Current: {}mA".format(battery_current))
        
                sign = normal.split('|')[2]
                print("sigh: {}".format(sign))
            
                if float(sign) < 0.0:
                    print("Battery is Charging...")
                else:
                    print("Power is consuming...")
            else:
                print("Empty: {}".format(normal))
        else:
                print("No Data")
                
        time.sleep(2)
        print("-"*38)
        
except ValueError:
    print("Value Error: {}".format(ValueError))
    STOP = True
    
except IndexError:
    print("Index Error: {}".format(IndexError))
    STOP = True
    
except KeyboardInterrupt:
    print('Got ctrl-c')
    STOP = True
